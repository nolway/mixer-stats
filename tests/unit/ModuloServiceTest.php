<?php

namespace App\Tests\Unit;

use App\Service\Math\ModuloService;
use PHPUnit\Framework\TestCase;

class ModuloServiceTest extends TestCase
{
    public function testCalc()
    {
        $firstNum = 5;
        $secondNum = 2;
        $service = new ModuloService();
        $result = $service->calc($firstNum, $secondNum);

        $this->assertSame(1.0, $result);
    }
}
