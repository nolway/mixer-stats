<?php

namespace App\Tests\Integration;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class GetTopViewsMixerTest extends TestCase
{
    public function getTopViewsMixerTest()
    {
        $client = new Client([
            'base_uri' => 'https://mixer.com/'
        ]);
        $response = $client->request('GET', 'api/v1/channels?order=viewersCurrent:DESC&limit=20&where=languageId:eq:fr');
        $this->assertEquals(200, $response->getStatusCode());
        $body = json_decode($response->getBody(), true);
        $this->assertIsArray($body);
        $this->assertGreaterThan(0, count($body));
    }
}
