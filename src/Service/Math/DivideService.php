<?php

namespace App\Service\Math;

use Exception;

/**
 * Class DivideService
 * @package App\Service\Math
 */
class DivideService
{
    public function __construct()
    {
    }
    /**
     * Return the result of the division operation
     *
     * @param float $number1
     * @param float $number2
     * @param bool $decimal
     * @return float
     * @throws Exception
     */
    public function calc(float $number1, float $number2, bool $decimal = true): float
    {
        if ($number2 == 0) {
            throw new Exception("Error ! Divide by zero !");
        }

        $res = $number1 / $number2;
        return (true === $decimal) ? $res : floor($res);
    }
}
